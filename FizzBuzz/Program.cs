﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {

            FizzBuzz fizzbuzz = new FizzBuzz(); 

            fizzbuzz.BeginTesting(); // When program is started all variables for counting are set to 0.
                        
            Console.WriteLine("9 is divisible by 9 : " + fizzbuzz.IsFizz(9));
            Console.WriteLine("14 is divisible by 13 : " + fizzbuzz.IsBuzz(14));
            Console.WriteLine("117 is divisible by 9 & 13 : " + fizzbuzz.IsBuzz(117));
            Console.WriteLine("7 is a prime number : " + fizzbuzz.IsPrime(7));
            Console.WriteLine("The number of Fizzs in this file is : " + fizzbuzz.TotalFizz());
            Console.WriteLine("The number of Buzzs in this file is : " + fizzbuzz.TotalBuzz());
            Console.WriteLine("The number of FizzBuzzs in this file is : " + fizzbuzz.TotalFizzBuzz());
            Console.WriteLine("The number of Primes in this file is : " + fizzbuzz.TotalPrime());

        }
    }
}
