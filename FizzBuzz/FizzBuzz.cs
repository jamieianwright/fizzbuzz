﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class FizzBuzz
    {

        public int NumberOfFizz { get; set; }
        public int NumberOfBuzz { get; set; }
        public int NumberOfFizzsBuzz { get; set; }
        public int NumberOfPrime { get; set; }

        public void BeginTesting() // Sets all variables to 0 to begin testing.
        {
            NumberOfFizz = 0;
            NumberOfBuzz = 0;
            NumberOfFizzsBuzz = 0;
            NumberOfPrime = 0;
        }

        StreamReader s1 = new StreamReader(@"C:\Users\jamie\source\repos\FizzBuzz\FizzBuzz\bin\Debug\FizzBuzzTest.txt");
        public int TotalFizz() //Counts number of "Fizz" numbers.
        {
            string number;
            int numOfFizzs = 0;
            {              
                while ((number = s1.ReadLine()) != null)
                {
                    if (IsFizz(int.Parse(number)) == true)
                    {
                        numOfFizzs++;
                    }
                }
                return NumberOfFizz = numOfFizzs;
            }
        }

        StreamReader s2 = new StreamReader(@"C:\Users\jamie\source\repos\FizzBuzz\FizzBuzz\bin\Debug\FizzBuzzTest.txt");
        public int TotalBuzz() //Counts number of "Buzz" numbers.
        {
            string number;
            int numOfBuzzs = 0;
            {
                while ((number = s2.ReadLine()) != null)
                {
                    if (IsBuzz(int.Parse(number)) == true)
                    {
                        numOfBuzzs++;
                    }
                }
                return NumberOfBuzz = numOfBuzzs;
            }
        }

        StreamReader s3 = new StreamReader(@"C:\Users\jamie\source\repos\FizzBuzz\FizzBuzz\bin\Debug\FizzBuzzTest.txt");
        public int TotalFizzBuzz() //Counts number of "FizzBuzz" numbers.
        {
            string number;
            int numOfFizzsBuzz = 0;
            {
                while ((number = s3.ReadLine()) != null)
                {
                    if (IsFizzBuzz(int.Parse(number)) == true)
                    {
                        numOfFizzsBuzz++;
                    }
                }
                return NumberOfFizzsBuzz = numOfFizzsBuzz;
            }
        }

        StreamReader s4 = new StreamReader(@"C:\Users\jamie\source\repos\FizzBuzz\FizzBuzz\bin\Debug\FizzBuzzTest.txt");
        public int TotalPrime() //Counts number of Prime numbers.
        {
            string number;
            int numOfPrime = 0;
            {
                while ((number = s4.ReadLine()) != null)
                {
                    if (IsPrime(int.Parse(number)) == true)
                    {
                        numOfPrime++;
                    }
                }
                return NumberOfPrime = numOfPrime;
            }
        }

        public bool IsFizz(int number) //Checks to see is input is divisible by 9.
        {
            return number % 9 == 0 ? true : false;
        }

        public bool IsBuzz(int input) //Checks to see is input is divisible by 13.
        {
            return input % 13 == 0 ? true:false;
        }

        public bool IsFizzBuzz(int input) //Checks to see if input is divisible by 9 & 13.
        {
            return IsFizz(input) && IsBuzz(input) ? true : false;
        }

        public bool IsPrime(int input) //Checks to see if input is a prime number.
        {
            if (input == 1) return false;
            if (input == 2) return true;

            for (int i=3; i<input; i+=2)
            {
                if(input % i ==0)
                {
                    return false;
                }                
            }
            return true;
        }
                
    }
}
